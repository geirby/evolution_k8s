# README #

### What is this repository for? ###
This is a the test from Evolution Gaming company

1. ✅ Create Kubernetes cluster using Vagrant in VirtualBox VM (or multiple VMs) using method you prefer
2. ✅ Install and configure Apache Kafka
3. ✅ Create 2 topics in Kafka called 'input' and 'output'
4. ✅ Create consumer and producer program in any programming language (preferable Golang or Python)
    - a. ✅ make producer continuously write messages to 'input' topic with epoch timestamp in MS
    - b. ✅ make consumer that reads from 'input' topic, transforms input message to date string (must be in RFC 3339) and sends to topic 'output'
5. ✅ Deploy both applications (producer and consumer) to k8s cluster
6. ✅ Install Prometheus, Grafana to k8s cluster
7. ⌚ Find a way to export Kafka metrics and metrics from applications (producer, consumer) to Prometheus and visualize them using Grafana dashboard (working on it)

#### Requirements: ####
1. ✅ VM, k8s cluster, Kafka, your application, Prometheus and Grafana deployment/provisioning/configuration MUST be automated (by any means, preferably Ansible)
2. ✅ Solution MUST NOT require installation of any additional packages on host machine except VirtualBox and Vagrant
3. ✅ Basic documentation MUST be provided (how-to connect to cluster, access metrics and so on)
4. ✅ Solution MUST run on host machine under any operating system
  
#### Manual ####
1. git clone https://geirby@bitbucket.org/geirby/evolution_k8s.git
2. cd evolution_k8s
3. vagrant up
4. vagrant ssh deploy_machine
5. check status
    ```bash 
    kubectl get pods | grep producer | awk '{print $1}' | xargs kubectl logs -f
    kubectl get pods | grep consumer | awk '{print $1}' | xargs kubectl logs -f
    ```
6. check Grafana:  https://10.0.0.11/grafana
    ```
    user: admin
    password: admin
    ```
7. check Prometheus metrics from producer/consumer https://10.0.0.11/targets#pool-kubernetes-pods  (in process)

#### Details about k8s: ####
Master node address: 10.0.0.10  
[Prometheus address](https://10.0.0.11/)  
[Grafana_1 address](https://10.0.0.11/grafana)  
[Grafana_2 address](https://10.0.0.12/grafana)
  
#### History: ####
Start testing:   **2021-06-01T14:00:00+03:00**  
Finish testing: **2021-06-08T08:00:00+03:00**
  