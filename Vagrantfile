Vagrant.configure("2") do |config|
  config.vm.provider :virtualbox do |v|
    v.memory = 2048
    v.cpus = 4
  end

  #config.vm.provision :shell, privileged: true, inline: $install_common_tools
config.vm.define :master do |master|
    master.vm.box = "ubuntu/bionic64"
    master.vm.hostname = "master"
    master.vm.network :private_network, ip: "10.0.0.10"
	master.vm.provision :shell, privileged: true, inline: $pre_script
    master.vm.provision :shell, privileged: true, inline: $install_k8s_repo
    master.vm.provision :shell, privileged: true, inline: $k8s_common_setup
    master.vm.provision :shell, privileged: false, inline: $provision_master_node
  end

  %w{worker1 worker2}.each_with_index do |name, i|
    config.vm.define name do |worker|
      worker.vm.box = "ubuntu/bionic64"
      worker.vm.hostname = name
      worker.vm.network :private_network, ip: "10.0.0.#{i + 11}"
	  worker.vm.provision :shell, privileged: true, inline: $pre_script
      worker.vm.provision :shell, privileged: true, inline: $install_k8s_repo
      worker.vm.provision :shell, privileged: true, inline: $k8s_common_setup
      worker.vm.provision :shell, privileged: false, inline: <<-SHELL
sudo /vagrant/.creds/join.sh
echo 'Environment="KUBELET_EXTRA_ARGS=--node-ip=10.0.0.#{i + 11} --resolv-conf=/run/systemd/resolve/resolv.conf"' | sudo tee -a /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
sudo systemctl daemon-reload
sudo systemctl restart kubelet

# Hack prometheus PV
sudo mkdir -p /data/prometheus-server-pv-1 && sudo chmod 777 /data/prometheus-server-pv-1 

SHELL
    end
  end

  config.vm.define :deploy_machine do |deploy_machine|
    deploy_machine.vm.box = "ubuntu/focal64"
    deploy_machine.vm.hostname = "deploy"
    deploy_machine.vm.network :private_network, ip: "10.0.0.254"
    deploy_machine.vm.provision :shell, privileged: true, inline: $pre_script
    deploy_machine.vm.provision :shell, privileged: true, inline: $install_k8s_repo
    deploy_machine.vm.provision :shell, privileged: false, inline: $provision_deploy_machine
    deploy_machine.vm.provider :virtualbox do |vb|
        vb.customize ["modifyvm", :id, "--memory", "1024"]
        vb.customize ["modifyvm", :id, "--cpus", "2"]
    end
    deploy_machine.vm.synced_folder "ansible/", "/home/vagrant/ansible"
    deploy_machine.vm.synced_folder "k8s/", "/home/vagrant/k8s"
  end
end

$pre_script = <<-SCRIPT
sed -ie 's/archive\.ubuntu\.com/mirror.yandex.ru/g' /etc/apt/sources.list
apt-get update 
SCRIPT

$install_k8s_repo = <<-SCRIPT
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get -qq update
SCRIPT

$k8s_common_setup = <<-SCRIPT
cat /vagrant/me.pub >> /home/vagrant/.ssh/authorized_keys

# bridged traffic to iptables is enabled for kube-router.
cat >> /etc/ufw/sysctl.conf <<EOF
net/bridge/bridge-nf-call-ip6tables = 1
net/bridge/bridge-nf-call-iptables = 1
net/bridge/bridge-nf-call-arptables = 1
EOF

# disable swap
swapoff -a
sed -i '/swap/d' /etc/fstab

# Install kubeadm, kubectl and kubelet
export DEBIAN_FRONTEND=noninteractive
apt-get -qq install ebtables ethtool
apt-get -qq update
apt-get -qq install -y docker.io apt-transport-https curl
apt-get -qq install -y kubelet kubeadm kubectl

SCRIPT

$provision_master_node = <<-SHELL
mkdir -p /vagrant/.creds
OUTPUT_FILE=/vagrant/.creds/join.sh
rm -rf $OUTPUT_FILE

# Start cluster
sudo kubeadm init --apiserver-advertise-address=10.0.0.10 --pod-network-cidr=10.244.0.0/16 | grep "kubeadm join" | awk '{print $0 "--discovery-token-unsafe-skip-ca-verification"}' > ${OUTPUT_FILE}
chmod +x $OUTPUT_FILE

# Configure kubectl
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

# Fix kubelet IP
echo 'Environment="KUBELET_EXTRA_ARGS=--node-ip=10.0.0.10 --resolv-conf=/run/systemd/resolve/resolv.conf"' | sudo tee -a /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

# Configure flannel
echo "Apply flannel"
kubectl apply -f /vagrant/k8s/kube-flannel.yml

# Configure dns
kubectl apply -f /vagrant/k8s/dns.yaml
kubectl -n kube-system rollout restart deployment coredns

sudo systemctl daemon-reload
sudo systemctl restart kubelet

kubectl config view --minify -o jsonpath='{.clusters[0].cluster.server}' > /vagrant/.creds/_K8S_API_SERVER
kubectl -n kube-system create serviceaccount ansible && \
  kubectl create clusterrolebinding ansible_role --clusterrole=cluster-admin --serviceaccount=kube-system:ansible && \
  kubectl get -n kube-system secret $(kubectl get -n kube-system serviceaccount ansible -o jsonpath='{.secrets[0].name}') -o jsonpath='{.data.token}' | base64 --decode > /vagrant/.creds/_K8S_TOKEN
SHELL

$install_multicast = <<-SHELL
apt-get -qq install -y avahi-daemon libnss-mdns
SHELL

$provision_deploy_machine = <<-SHELL
export KAFKA_TOPIC=input
export KAFKA_TOPIC_OUTPUT=output

cat /vagrant/me.pub >> /home/vagrant/.ssh/authorized_keys

sudo apt-get update && sudo apt-get -y upgrade && sudo apt-get -y install vim ansible git curl wget python3-pip kubectl

# Setup k8s creds
export API_SERVER=$(cat /vagrant/.creds/_K8S_API_SERVER)
export TOKEN=$(cat /vagrant/.creds/_K8S_TOKEN)
export API_SERVER_CONF="K8S_AUTH_HOST=$API_SERVER"
export TOKEN_CONF="K8S_AUTH_API_KEY=$TOKEN"
if [ ! "$(grep bashrc_k8s $HOME/.bashrc)" ]; then echo ". $HOME/.bashrc_k8s" >> "$HOME/.bashrc"; fi
echo "export K8S_AUTH_VERIFY_SSL=false" > "$HOME/.bashrc_k8s"
echo "export $API_SERVER_CONF" >> "$HOME/.bashrc_k8s"
echo "export $TOKEN_CONF" >> "$HOME/.bashrc_k8s"
kubectl config set-cluster default --server=$API_SERVER --insecure-skip-tls-verify=true
kubectl config set-credentials ansible --token=$TOKEN
kubectl config set-context default-context --cluster=default --user=ansible
kubectl config use-context default-context

ansible-galaxy collection install community.kubernetes
pip install openshift pyyaml kubernetes

# Install helm
curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
sudo apt-get install apt-transport-https --yes
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update && sudo apt-get -y install helm
helm plugin install https://github.com/databus23/helm-diff

# Deploy PV
kubectl apply -f /vagrant/k8s/pv.yaml
source "$HOME/.bashrc_k8s"

# Deploy kafka
cd /vagrant/ansible && ansible-playbook install_kafka.yml -D 
kubectl wait --for=condition=Ready --timeout 400s pod/kafka-0 && kubectl exec kafka-0 bash -- /opt/bitnami/kafka/bin/kafka-topics.sh --create --zookeeper kafka-zookeeper:2181 --topic "$KAFKA_TOPIC" --partitions 1 --replication-factor 1 
kubectl exec kafka-0 bash -- /opt/bitnami/kafka/bin/kafka-topics.sh --create --zookeeper kafka-zookeeper:2181 --topic output --partitions 1 --replication-factor 1
cd /vagrant/ansible && ansible-playbook install_ev_producer.yml && ansible-playbook install_ev_consumer.yml

# Install ingress
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install --set controller.hostNetwork=true,controller.service.type=NodePort,prometheus.create=true,controller.kind=DaemonSet ingress-nginx ingress-nginx/ingress-nginx

kubectl wait --namespace default \
--for=condition=ready pod \
--selector=app.kubernetes.io/component=controller \
--timeout=400s

# Deploy Prometheus
cd /vagrant/ansible && ansible-playbook install_prometheus.yml -D 

# Deploy grafana
cd /vagrant/ansible && ansible-playbook install_grafana.yml -D 


SHELL
